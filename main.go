package main

import (
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/joho/sqltocsv"
	"os"
)

var DBClient *sqlx.DB

type Config struct {
	Database struct {
		Username      string `json:"username"`
		Password      string `json:"password"`
		Protocol      string `json:"protocol"`
		ServerAddress string `json:"server_address"`
		DatabaseName  string `json:"database_name"`
	} `json:"database"`
	Queries []struct {
		Query    string `json:"query"`
		FileName string `json:"filename"`
	} `json:"queries"`
}

func loadConfiguration(filename string) Config {
	var config Config
	configFile, err := os.Open(filename)
	defer configFile.Close()
	if err != nil {
		panic(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	if err != nil {
		panic(err.Error())
	}
	return config
}

func main() {
	var configFile string
	fmt.Println("Please type file path of config file: ")
	fmt.Scanln(&configFile)
	config := loadConfiguration(configFile)
	initializeDatabase(config)
	writeQuery(config)
}

func initializeDatabase(config Config) { // establishes connection with SQL database
	var sourceName = config.Database.Username + ":" + config.Database.Password + "@" + config.Database.Protocol + "(" + config.Database.ServerAddress + ")/" + config.Database.DatabaseName
	db, err := sqlx.Open("mysql", sourceName)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("Link to database established...")
	DBClient = db // creates database
}

func writeQuery(config Config) { // takes query from client and saves results as a csv file
	for i := 0; i < len(config.Queries); i++ {
		rows, err := DBClient.Query(config.Queries[i].Query) // performs query on SQL table
		if err != nil {
			fmt.Println("Error while grabbing query ")
			panic(err.Error())
		}
		_, err = os.Create(config.Queries[i].FileName) // create .csv file
		if err != nil {
			panic(err.Error())
		}
		err = sqltocsv.WriteFile(config.Queries[i].FileName, rows) // outputs query to csv
		if err != nil {
			panic(err.Error())
		}
	}
}
