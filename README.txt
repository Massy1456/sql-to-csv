To run the program simply double click 'sql-to-csv.exe'
Once it asks you for the file path, insert the path of where your config file is located. 
(this can be of type *.json or *.txt)

Example: C:\Users\maxce\Desktop\sql-to-csv\config.txt
Example: C:\Users\maxce\Desktop\sql-to-csv\config.json

Once you do this, it will output a '*.csv' file in the same folder as where you are running your 'sql-to-csv.exe'

Change the information on the config file to match what you need to query from the database.
